def factors(x):

    # ==============
    # Your code here
    for i in range(1, x + 1):
        if x % i == 0:
            print(i)
    # ==============

print("The factors of 15 are: ")
factors(15)
print()
# Should print [3, 5] to the console

print("The factors of 15 are: ")
factors(12)
print()
# Should print [2, 3, 4, 6] to the console

print("The factors of 15 are: ")
factors(13)
print()
# Should print “[]” (an empty list) to the console

print("NOTE - Have not figured out how to format the output of the for loop into a list as of yet.")
