def vowel_swapper(string):
    # ==============
    # Your code here
    return string.replace('a' and 'A', '4', 1).replace('e' and 'E', '3', 1).replace('i' and 'I', '!', 1).replace(
        'o' and 'O', 'ooo' and 'OOO', 1).replace('u' and 'U', '|_|', 1)
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
